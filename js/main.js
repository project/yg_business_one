(function ($) {

	Drupal.behaviors.exampleModule = {
  	attach: function (context, settings) {

 	'use strict';

jQuery(document).ready(function( $ ) {
  $('.loop').owlCarousel({
    loop: true,
    margin: 10,
    responsive: {
      0:{
	        items:1
	    },
	    600:{
	        items:1
	    },
	    1000:{
	        items:1
	    }    
    }
  });
  new WOW().init();
});

$('.dropdown-menu span.dropdown-toggle').on('click', function(e) {
	if (!$(this).next().hasClass('show')) {
	  $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	}
	var $subMenu = $(this).next(".dropdown-menu");
	$subMenu.toggleClass('show');

	$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	  $('.dropdown-submenu .show').removeClass("show");
	});

	return false;
});
 $(".nav-menu li:first-child").addClass("menu-active");

 if ($("section#content").hasClass("main-content")) {
	$("section#content").removeClass("col");
}
 
}}})(jQuery, Drupal);